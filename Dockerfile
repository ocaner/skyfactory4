#FROM openjdk:8
FROM arm64v8/openjdk:8

ARG TARGET_DIR=/opt
ARG SF4_DIR=$TARGET_DIR/sf4-server

ENV EULA=false
ENV SF4_DIR=$SF4_DIR
ENV PATH=$SF4_DIR:$PATH
ENV MIN_RAM=2048M
ENV MAX_RAM=4096M

COPY files/sf4-server $SF4_DIR
COPY patches/settings.sh $SF4_DIR/

RUN echo "*** SKY FACTORY 4 - SERVER ***" \ 
    && env \
    && echo "SF4_DIR = $SF4_DIR" \
    && cd $SF4_DIR \
    && chmod +x $SF4_DIR/*.sh \
    && ls -la $SF4_DIR/* \
    && $SF4_DIR/Install.sh \
    && mkdir -p $SF4_DIR/world

WORKDIR $SF4_DIR

VOLUME $SF4_DIR/world

ENTRYPOINT $SF4_DIR/ServerStart.sh
